﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GlobalModels
{
    [Table("Users")]
    public class User
    {


        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }


        public string UserName { get; set; }
        public string Email { get; set; }

        public int AccountType { get; set; }
        public float Balance { get; set; }

        public string Firstname { get; set; }
        public string Surname { get; set; }

        public int? Sex { get; set; }


        public int? MainPhotoId { get; set; }


        public string PostalCode { get; set; }
        public string Street { get; set; }
        public string Country { get; set; }
        public string City { get; set; }

        public long? PESEL { get; set; }

        public DateTime? BirthDate { get; set; }


        public string CompanyName { get; set; }
        public string NIP { get; set; }
        public string REGON { get; set; }
        public string CompanyCountry { get; set; }
        public string CompanyCity { get; set; }
        public string CompanyPostalCode { get; set; }
        public string CompanyStreet { get; set; }



        public bool Activated { get; set; }
        public DateTime? ActivationDate { get; set; }
        public string ActivationCode { get; set; }

        public DateTime? CreateDate { get; set; }
        public bool del { get; set; }

        public bool acceptMarketing { get; set; }

        public int gahPoints { get; set; }

        //    public virtual ICollection<GameLog> gahLogs { get; set; }

        //    public virtual IEnumerable<GameLog> gahLogsToReturn
        //    {
        //        get
        //        {
        //            return this.gahLogs.Where(x => x.del != true);
        //        }
        //        set { }
        //    }

        //    public virtual ICollection<ReferralRegistration> UserReferralRegistrations { get; set; }

        //    public virtual IEnumerable<ReferralRegistration> UserReferralRegistrationsToReturn
        //    {
        //        get
        //        {
        //            if (this.UserReferralRegistrations != null)
        //                return this.UserReferralRegistrations.Where(x => x.del != true && x.UserReferred.Activated == true);
        //            else
        //                return null;
        //        }
        //        set { }
        //    }



        //    //[ForeignKey("MainPhotoId")]
        //    public virtual Attachment Photo { get; set; }
        //}
    }
}
