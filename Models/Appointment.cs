﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GlobalModels
{
    [Table("Appointments")]
    public class Appointment
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int appointmentId { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public virtual User medicineDoctor { get; set; }
        public virtual User patient { get; set; }


    }
}
