﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace GlobalModels
{
    [Table("ConsultingHours")]
    public class ConsultingHours
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int consultingHoursId { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public int medicineDoctorId { get; set; }
        

    }
}
