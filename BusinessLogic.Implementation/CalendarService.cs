﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Contracts;
using DataAccessLayer.Contracts;
using CalendarAndVidyo;
using GlobalModels;

namespace BusinessLogic.Implementation
{
    public class CalendarService:ICalendarService
    {
        private IConsultingHoursRepository _consultigHoursRepository;
        private IUnitOfWork _unitOfWork;
        private IUserRepository _userRepository;
        public CalendarService(IConsultingHoursRepository consultingHoursRepository,IUserRepository userRepository,IUnitOfWork unitOfWork)
        {
            _consultigHoursRepository = consultingHoursRepository;
            _userRepository = userRepository;
            _unitOfWork= unitOfWork;
        }
        public IEnumerable<CalendarAndVidyo.EventViewModel> GetEventsForDoctorByDoctorId(int doctorId)
        {
            return _consultigHoursRepository
                .GetConsultingHours(doctorId)
                .Select(ch => new EventViewModel() { start_date = ch.startDate, end_date = ch.endDate, id = ch.consultingHoursId, text = "Godziny przyjęć" })
                .ToList();
        }

        public void UpdateDoctorEvent(CalendarAndVidyo.EventViewModel updatedEvent, int doctorId)
        {

            ConsultingHours updatedConsultingHours = EventViewModelToConsultingHours(updatedEvent, doctorId);
            updatedConsultingHours.consultingHoursId = updatedEvent.id;
            _unitOfWork.MarkAsChanged(updatedConsultingHours);
            _unitOfWork.Commit();
        }

        public void DeleteDoctorEvent(CalendarAndVidyo.EventViewModel deletedEvent, int doctorId)
        {
            ConsultingHours deletedConsultingHours = EventViewModelToConsultingHours(deletedEvent, doctorId);
            deletedConsultingHours.consultingHoursId = deletedEvent.id;
            _consultigHoursRepository.DeleteConsultingHours(deletedConsultingHours,_unitOfWork);
            _unitOfWork.Commit();
        }

        public void AddDoctorEvent(CalendarAndVidyo.EventViewModel newEvent, int doctorId)
        {

            ConsultingHours newConsultingHours = EventViewModelToConsultingHours(newEvent, doctorId);
            _consultigHoursRepository.AddConsultingHours(newConsultingHours);
            _unitOfWork.Commit();
        }

        private static ConsultingHours EventViewModelToConsultingHours(CalendarAndVidyo.EventViewModel newEvent, int doctorId)
        {
            ConsultingHours newConsultingHours = new ConsultingHours()
            {
                startDate = newEvent.start_date,
                endDate = newEvent.end_date,
                medicineDoctorId = doctorId
            };
            return newConsultingHours;
        }
    }
}
