﻿using BusinessLogic.Contracts;
using DataAccessLayer.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Implementation
{
    public class AccountService:IAccountService
    {
        private IUserRepository _userRepository;
        public AccountService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public GlobalModels.User GetUserByUserName(string usernName)
        {
            return _userRepository.GetUserByName(usernName);
        }
    }
}
