﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Contracts;
using System.Data.Entity;

namespace DataAccessLayer.Implementation
{
    public class UnitOfWork:IUnitOfWork
    {
        private ITakesCareContext _takesCareContext;
        public UnitOfWork(ITakesCareContext cc)
        {
            this._takesCareContext = cc;
        }
        private DbContext DbContext
        {
            get { return (DbContext)takesCareContext; } 
        }
        public ITakesCareContext takesCareContext
        { get { return _takesCareContext; } }
        

        public void Commit()
        {
            DbContext.SaveChanges();
        }

        public void Dispose()
        {
            //throw new NotImplementedException();
        }



        public void MarkAsChanged(object entity)
        {
            DbContext.Entry(entity).State = EntityState.Modified;
        }


        public void MarkAsDeleted(object entity)
        {
            DbContext.Entry(entity).State = EntityState.Deleted;
        }
    }
}
