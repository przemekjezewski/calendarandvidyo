﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Contracts;
using System.Data.Entity;

namespace DataAccessLayer.Implementation
{
    public class ConsultingHoursRepository: IConsultingHoursRepository
    {
        private ITakesCareContext _takesCareContext;
        private IUnitOfWork _unitOfWork;
        public ConsultingHoursRepository(IUnitOfWork uow)
        {
            _takesCareContext = uow.takesCareContext;
        }
        public IEnumerable<GlobalModels.ConsultingHours> GetConsultingHours()
        {
            return _takesCareContext.ConsultingHours
                .ToList();
        }

        public IEnumerable<GlobalModels.ConsultingHours> GetConsultingHours(DateTime From, DateTime To)
        {
            return _takesCareContext.ConsultingHours
                .Where(c => c.startDate < To && c.endDate > From)
                .ToList();
        }

        public IEnumerable<GlobalModels.ConsultingHours> GetConsultingHours(int medicineDoctorId)
        {
            return _takesCareContext.ConsultingHours
                .Where(c => c.medicineDoctorId == medicineDoctorId)
                .ToList();
        }

        public IEnumerable<GlobalModels.ConsultingHours> GetConsultingHours(int medicineDoctorId, DateTime From, DateTime To)
        {
            return _takesCareContext.ConsultingHours
                .Where(c => c.medicineDoctorId == medicineDoctorId && c.startDate < To && c.endDate > From)
                .ToList();
        }

        public void AddConsultingHours(GlobalModels.ConsultingHours consultingHours)
        {
            GlobalModels.ConsultingHours newConsultingHours = new GlobalModels.ConsultingHours() 
            {
                startDate=consultingHours.startDate,
                endDate=consultingHours.endDate,
                medicineDoctorId=consultingHours.medicineDoctorId,
                
            };
            
            _takesCareContext.ConsultingHours.Add(newConsultingHours);
            
        }

        public GlobalModels.ConsultingHours GetConsultingHoursById(int id)
        {
            return _takesCareContext.ConsultingHours
                .SingleOrDefault(c => c.consultingHoursId == id);
        }




        public void UpdateConsultingHours(GlobalModels.ConsultingHours consultingHours)
        {
           // ((DbContext)_takesCareContext).Entry(consultingHours).State = EntityState.Modified;
        }
        public void Save()
        { }


        public void DeleteConsultingHours(GlobalModels.ConsultingHours consultigHours,IUnitOfWork uow)
        {
            _takesCareContext = uow.takesCareContext;
            
           
            uow.MarkAsDeleted(consultigHours);
        }
    }
}
