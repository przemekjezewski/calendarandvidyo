﻿using DataAccessLayer.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Implementation
{
    public class UserRepository:IUserRepository
    {
        private ITakesCareContext _tacesCareContext;
        public UserRepository(ITakesCareContext takesCareContext)
        {
            _tacesCareContext = takesCareContext;
        }
        public void AddUser(GlobalModels.User user)
        {
            throw new NotImplementedException();
        }

        public GlobalModels.User GetUserByEmail(string Email)
        {
            throw new NotImplementedException();
        }

        public GlobalModels.User GetUserById(int UserId)
        {
            throw new NotImplementedException();
        }

        public GlobalModels.User GetUserByName(string UserName)
        {
            return _tacesCareContext.Users.SingleOrDefault(u => u.UserName == UserName);
        }

        public IEnumerable<GlobalModels.User> GetUsers()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<GlobalModels.User> GetMedicineDoctors()
        {
            throw new NotImplementedException();
        }
    }
}
