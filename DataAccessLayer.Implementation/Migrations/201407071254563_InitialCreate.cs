namespace DataAccessLayer.Implementation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Appointments",
                c => new
                    {
                        appointmentId = c.Int(nullable: false, identity: true),
                        startDate = c.DateTime(nullable: false),
                        endDate = c.DateTime(nullable: false),
                        medicineDoctor_UserId = c.Int(),
                        patient_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.appointmentId)
                .ForeignKey("dbo.Users", t => t.medicineDoctor_UserId)
                .ForeignKey("dbo.Users", t => t.patient_UserId)
                .Index(t => t.medicineDoctor_UserId)
                .Index(t => t.patient_UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        Email = c.String(),
                        AccountType = c.Int(nullable: false),
                        Balance = c.Single(nullable: false),
                        Firstname = c.String(),
                        Surname = c.String(),
                        Sex = c.Int(),
                        MainPhotoId = c.Int(),
                        PostalCode = c.String(),
                        Street = c.String(),
                        Country = c.String(),
                        City = c.String(),
                        PESEL = c.Long(),
                        BirthDate = c.DateTime(),
                        CompanyName = c.String(),
                        NIP = c.String(),
                        REGON = c.String(),
                        CompanyCountry = c.String(),
                        CompanyCity = c.String(),
                        CompanyPostalCode = c.String(),
                        CompanyStreet = c.String(),
                        Activated = c.Boolean(nullable: false),
                        ActivationDate = c.DateTime(),
                        ActivationCode = c.String(),
                        CreateDate = c.DateTime(),
                        del = c.Boolean(nullable: false),
                        acceptMarketing = c.Boolean(nullable: false),
                        gahPoints = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.ConsultingHours",
                c => new
                    {
                        consultingHoursId = c.Int(nullable: false, identity: true),
                        startDate = c.DateTime(nullable: false),
                        endDate = c.DateTime(nullable: false),
                        medicineDoctor_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.consultingHoursId)
                .ForeignKey("dbo.Users", t => t.medicineDoctor_UserId)
                .Index(t => t.medicineDoctor_UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ConsultingHours", "medicineDoctor_UserId", "dbo.Users");
            DropForeignKey("dbo.Appointments", "patient_UserId", "dbo.Users");
            DropForeignKey("dbo.Appointments", "medicineDoctor_UserId", "dbo.Users");
            DropIndex("dbo.ConsultingHours", new[] { "medicineDoctor_UserId" });
            DropIndex("dbo.Appointments", new[] { "patient_UserId" });
            DropIndex("dbo.Appointments", new[] { "medicineDoctor_UserId" });
            DropTable("dbo.ConsultingHours");
            DropTable("dbo.Users");
            DropTable("dbo.Appointments");
        }
    }
}
