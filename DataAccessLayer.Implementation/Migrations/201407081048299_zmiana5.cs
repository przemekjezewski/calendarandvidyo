namespace DataAccessLayer.Implementation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class zmiana5 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ConsultingHours", "medicineDoctor_UserId", "dbo.Users");
            DropIndex("dbo.ConsultingHours", new[] { "medicineDoctor_UserId" });
            AddColumn("dbo.ConsultingHours", "medicineDoctorId", c => c.Int(nullable: false));
            DropColumn("dbo.ConsultingHours", "medicineDoctor_UserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ConsultingHours", "medicineDoctor_UserId", c => c.Int());
            DropColumn("dbo.ConsultingHours", "medicineDoctorId");
            CreateIndex("dbo.ConsultingHours", "medicineDoctor_UserId");
            AddForeignKey("dbo.ConsultingHours", "medicineDoctor_UserId", "dbo.Users", "UserId");
        }
    }
}
