﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using DataAccessLayer.Contracts;
using GlobalModels;

namespace DataAccessLayer.Implementation
{
    public class TakesCareContext : DbContext, ITakesCareContext
    {
         public TakesCareContext()
            : base("DefaultConnection") { }


         public DbSet<ConsultingHours> ConsultingHours {get;set;}
        
         public DbSet<Appointment> Appointments{get;set;}

         public DbSet<User> Users { get; set; }
         
    }
}
