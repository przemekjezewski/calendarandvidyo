﻿using System;


namespace BusinessLogic.Contracts
{
    public interface IAccountService
    {
        GlobalModels.User GetUserByUserName(string usernName);
    }
}
