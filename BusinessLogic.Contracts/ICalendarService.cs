﻿using System;


namespace BusinessLogic.Contracts
{
   public interface ICalendarService
    {
        System.Collections.Generic.IEnumerable<CalendarAndVidyo.EventViewModel> GetEventsForDoctorByDoctorId(int doctorId);
        void UpdateDoctorEvent(CalendarAndVidyo.EventViewModel updatedEvent,int doctorId);
        void DeleteDoctorEvent(CalendarAndVidyo.EventViewModel deletedEvent,int doctorId);
        void AddDoctorEvent(CalendarAndVidyo.EventViewModel newEvent, int doctorId);

    }
}
