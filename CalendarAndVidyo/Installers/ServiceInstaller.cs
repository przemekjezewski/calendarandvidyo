﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalendarAndVidyo.Installers
{
    public class ServiceInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register((Classes.FromAssemblyNamed("BusinessLogic.Implementation")
                                .Where(Component.IsInSameNamespaceAs<BusinessLogic.Implementation.CalendarService>())
                                .WithService.DefaultInterfaces()
                                .LifestylePerWebRequest()));




        }
    }
}