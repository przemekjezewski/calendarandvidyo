﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CalendarAndVidyo.Startup))]
namespace CalendarAndVidyo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
