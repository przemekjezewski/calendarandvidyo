﻿using DHTMLX.Scheduler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalendarAndVidyo.ViewModels
{
    public class ViewModel
    {
        public DHXScheduler Scheduler { get; set; }
        public int CategoryCount { get; set; }
    }
    public class FormState
    {
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string TimeFrom { get; set; }
        public string TimeTo { get; set; }
        public string Type { get; set; }
        public string Price { get; set; }
        public bool DateFilter { get; set; }
    }
}