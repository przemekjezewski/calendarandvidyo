﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using System.Collections.Generic;
using DHTMLX.Scheduler;
using DHTMLX.Scheduler.Controls;
using DHTMLX.Scheduler.Data;
using DHTMLX.Common;
using GlobalModels;
using System;
using System.Security;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using System.Globalization;
using DataAccessLayer.Implementation;
using CalendarAndVidyo.ViewModels;


namespace CalendarAndVidyo.Controllers
{
    public class PatientCalendarController : Controller
    {
        
        public ActionResult Index(FormState state)
        {
           
            var scheduler = new DHXScheduler(this);
            scheduler.Extensions.Add(SchedulerExtensions.Extension.Collision);
            scheduler.Extensions.Add(SchedulerExtensions.Extension.Minical);

            scheduler.Skin = DHXScheduler.Skins.Terrace;

            //call custom template initialization
            scheduler.BeforeInit.Add("def_template();");

            scheduler.Config.time_step = 15;      
            //set row height
            scheduler.XY.bar_height = 76;

            scheduler.Config.wide_form = true;
            var lang = SchedulerLocalization.Localizations.Polish;
            scheduler.Localization.Set(lang, false);
            scheduler.InitialValues.Add("text", "New Contact");

            //if 'Pick Up Date' selected - make it initial date for calendar
            if (_ParseDate(state.DateFrom, state.TimeFrom) != default(DateTime))
            {              
                scheduler.InitialDate = _ParseDate(state.DateFrom, state.TimeFrom);
            }

            var context = new TakesCareContext();

   

            //selecting cars according to form values
            var cars = _SelectCars(context, state);
  
            //if no cars found - show message and load default set
            if (cars.Count() == 0){
                ViewData["Message"] = "Nie znaleziono wolnych terminów spełniających Twoje wymagania.";
                cars = _SelectCars(context);//select default set of events
            }

            //create custom details form
            var printableList = context.Users.Where(u => u.AccountType == 1).Select(u => new { key = u.UserId, label = u.Firstname + " " + u.Surname, price = u.UserName }).ToList();
                //new List<object> {new { key = 2, label = "cos", price = 50},new { key = 3, label = "cos", price = 60}};
                //cars.Select(c => new { key = c.id, label = "coś", price = c.Price}).ToList();
            _ConfigureLightbox(scheduler, printableList);
            //load cars to the timeline view
            _ConfigureViews(scheduler, printableList);
           
            //assign ViewData values
            _UpdateViewData(scheduler, context, state);
            
            //data loading/saving settings
            scheduler.PreventCache();
            scheduler.LoadData = true;
            scheduler.EnableDataprocessor = true;

            //collect model
            var model = new ViewModel();
            model.Scheduler = scheduler;
            model.CategoryCount = cars.Count();
            return View(model);
        }


     
        protected void _UpdateViewData(DHXScheduler scheduler, TakesCareContext context, FormState state)
        {
            ViewData["Price"] = _CreatePriceSelect(scheduler , state.Price);
            //ViewData["Type"] = _CreateTypeSelect(context.Types, state.Type) ;
            ViewData["DateFrom"] = state.DateFrom;
            ViewData["DateTo"] = state.DateTo;
            ViewData["TimeFrom"] = _CreateTimeSelect(scheduler, state.TimeFrom);
            ViewData["TimeTo"] = _CreateTimeSelect(scheduler, state.TimeTo);
            ViewData["DateFilter"] = state.DateFilter;
        }

        //private List<SelectListItem> _CreateTypeSelect(IEnumerable<Models.Type> types, string selected)
        //{
            
        //    var typesList = new List<SelectListItem>()
        //    {
        //        new SelectListItem(){Value = "", Text = "Any"}
        //    };
        //    foreach (var type in types)
        //    {
        //        var item = new SelectListItem() { Value = type.id.ToString(), Text = string.Format("{0}: {1} cars", type.title, type.Cars.Count) };
        //        if (item.Value == selected)
        //            item.Selected = true;
        //        typesList.Add(item);
                
        //    }
        //    return typesList;
        //}
        private List<SelectListItem> _CreatePriceSelect(DHXScheduler scheduler, string selected)
        {
            var priceRanges = new string[] { "50-80", "80-120", "120-150" };
            var prices = new List<SelectListItem>(){
                new SelectListItem(){Value = "", Text = "Any"}
            };

            foreach (var pr in priceRanges)
            {
                var item = new SelectListItem() { Value = pr, Text = string.Format("${0}", pr) };
                if (pr == selected)
                    item.Selected = true;
                prices.Add(item);
            }
            return prices;
        }
        private List<SelectListItem> _CreateTimeSelect(DHXScheduler scheduler, string selected)
        {
           
            var opts = new List<SelectListItem>();
            for (var i = scheduler.Config.first_hour; i < scheduler.Config.last_hour; i++)
            {
                var value = string.Format("{0}:00", i < 10 ? "0"+i.ToString() : i.ToString());
                var item = new SelectListItem() { Text = value, Value = value };
                if (value == selected)
                    item.Selected = true;
                opts.Add(item);
            }
            return opts;
        }

       


        private DateTime _ParseDate(string date, string time)
        {
            var datestr = string.Format("{0} {1}", date, time);
            DateTime result = new DateTime();
            DateTime.TryParse(datestr, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out result);
            return result;
        }
        protected IQueryable<User> _SelectCars(TakesCareContext context)
        {
            return _SelectCars(context, null);
        }

        protected IQueryable<User> _SelectCars(TakesCareContext context, FormState state)
        {
            IQueryable<User> docs = context.Users;
            

            return docs;
        }

        protected void _ConfigureViews(DHXScheduler scheduler, IEnumerable cars)
        {
            var units = new TimelineView("Orders", "car_id");
            units.X_Step = 2;
            units.X_Length = 12;
            units.X_Size = 12;
            //width of the first column
            units.Dx = 149;
            //row height
            units.Dy = 76;
            //order bar height
            units.EventDy = units.Dy - 5;
            units.AddOptions(cars);
            units.RenderMode = TimelineView.RenderModes.Bar;
            scheduler.Views.Clear();
            scheduler.Views.Add(units);
            scheduler.InitialView = scheduler.Views[0].Name;
        }

        protected void _ConfigureLightbox(DHXScheduler scheduler, IEnumerable cars)
        {
            scheduler.Lightbox.Add(new LightboxText("text", "Contact details") { Height = 42, Focus = true });
            scheduler.Lightbox.Add(new LightboxText("description", "Note") { Height = 63 });
            var select = new LightboxSelect("car_id", "Car Brand");
            scheduler.Lightbox.Add(select);
            scheduler.Lightbox.Add(new LightboxText("pick_location", "Pick up location") { Height = 21 });
            scheduler.Lightbox.Add(new LightboxText("drop_location", "Drop off location") { Height = 21 });

            select.AddOptions(cars);
            scheduler.Lightbox.Add(new LightboxTime("time", "Time period"));
        }



        public SchedulerAjaxData Data()
        {
            return new SchedulerAjaxData((new TakesCareContext()).Appointments);
        }



        public ContentResult Save(int? id, FormCollection actionValues)
        {

            var action = new DataAction(actionValues);

            TakesCareContext data = new TakesCareContext();
            try
            {

                var changedEvent = (Appointment)DHXEventsHelper.Bind(typeof(Appointment), actionValues);
                switch (action.Type)
                {
                    case DataActionTypes.Insert:
                        //data.Orders.InsertOnSubmit(changedEvent);
                        break;
                    case DataActionTypes.Delete:
                        //changedEvent = data.Orders.SingleOrDefault(ev => ev.id == action.SourceId);
                        //data.Orders.DeleteOnSubmit(changedEvent);
                        break;
                    default:// "update"                          
                        //var eventToUpdate = data.Orders.SingleOrDefault(ev => ev.id == action.SourceId);
                       // DHXEventsHelper.Update(eventToUpdate, changedEvent, new List<string>() { "id" });
                        break;
                }
                data.SaveChanges();
                action.TargetId = changedEvent.appointmentId;
            }
            catch
            {
                action.Type = DataActionTypes.Error;
            }

            return (new AjaxSaveResponse(action));
        }
    }
    }
