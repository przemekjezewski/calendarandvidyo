﻿using BusinessLogic.Contracts;
using DataAccessLayer.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;




namespace CalendarAndVidyo.Controllers
{
    
    public class HomeController : Controller
    {
        private IAccountService _accountService;
        public HomeController(IAccountService ias)
        {
            _accountService = ias;
        }
        public ActionResult Index()
        {
            if(!User.Identity.IsAuthenticated)  
            //return RedirectToAction("Index", "DoctorsCalendar");
            return View();
            else
            {
                string userName = User.Identity.Name;
                GlobalModels.User user = _accountService.GetUserByUserName(userName);
                if (user.AccountType == 1)
                    return RedirectToAction("Index", "DoctorsCalendar");
                else
                    return RedirectToAction("Index", "PatientCalendar");
                
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}