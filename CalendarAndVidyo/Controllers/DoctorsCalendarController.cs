﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DHTMLX.Scheduler;
using DHTMLX.Scheduler.Data;
using DHTMLX.Common;
using DataAccessLayer.Implementation;
using GlobalModels;
using System.Data.Entity;
using BusinessLogic.Contracts;

namespace CalendarAndVidyo.Controllers
{
    public class DoctorsCalendarController : Controller
    {
        private ICalendarService _calendarService;
        private IAccountService _AccountService;
        private int _doctorId;
        public DoctorsCalendarController(ICalendarService ics,IAccountService ias)
        {

            _AccountService = ias;
            _calendarService = ics;
            
        }

        private void InitializeDoctorsId()
        {
            var user =_AccountService.GetUserByUserName(User.Identity.Name);
            _doctorId = user.UserId;
        }
        public ActionResult Index()
        {
            InitializeDoctorsId();
            var scheduler = new DHXScheduler(this); //initializes dhtmlxScheduler
            scheduler.LoadData = true;// allows loading data
            scheduler.Config.time_step = 15;
            var lang = SchedulerLocalization.Localizations.Polish;
            scheduler.Localization.Set(lang, false);
            scheduler.EnableDataprocessor = true;// enables DataProcessor in order to enable implementation CRUD operations
            return View(scheduler);
        }

        public SchedulerAjaxData Data()
        {//events for loading to scheduler
            InitializeDoctorsId();
            IEnumerable<EventViewModel> events = _calendarService.GetEventsForDoctorByDoctorId(_doctorId);
            var sad =new SchedulerAjaxData(events);
            
            return sad;
        }

        public ActionResult Save(EventViewModel updatedEvent, FormCollection formData)
        {
            InitializeDoctorsId();
            var action = new DataAction(formData);
           

            try
            {
                switch (action.Type)
                {
                    case DataActionTypes.Insert: // your Insert logic
                        _calendarService.AddDoctorEvent(updatedEvent, _doctorId);
                       // context.Events.InsertOnSubmit(updatedEvent);
                        break;
                    case DataActionTypes.Delete: // your Delete logic
                        _calendarService.DeleteDoctorEvent(updatedEvent, _doctorId);
                        
                        break;
                    default:// "update" // your Update logic
                        _calendarService.UpdateDoctorEvent(updatedEvent, _doctorId);
                        break;
                }
                
                action.TargetId = updatedEvent.id;
            }
            catch (Exception a)
            {
                action.Type = DataActionTypes.Error;
            }
            return (new AjaxSaveResponse(action));
        }
	}
}