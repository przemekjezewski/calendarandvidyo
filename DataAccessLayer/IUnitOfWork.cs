﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Contracts
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();
        void MarkAsChanged(object entity);
        void MarkAsDeleted(object entity);
        
        ITakesCareContext takesCareContext{get;}
    }
    
}
