﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Contracts
{
    public interface IUserRepository
    {
        void AddUser(GlobalModels.User user);
        GlobalModels.User GetUserByEmail(string Email);
        GlobalModels.User GetUserById(int UserId);
        GlobalModels.User GetUserByName(string UserName);
        System.Collections.Generic.IEnumerable<GlobalModels.User> GetUsers();
        System.Collections.Generic.IEnumerable<GlobalModels.User> GetMedicineDoctors();
        

        //void Save();
    }
}
