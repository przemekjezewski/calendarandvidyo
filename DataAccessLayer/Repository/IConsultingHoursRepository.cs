﻿using System;


namespace DataAccessLayer.Contracts
{
    public interface IConsultingHoursRepository
    {
        System.Collections.Generic.IEnumerable<GlobalModels.ConsultingHours> GetConsultingHours();
        System.Collections.Generic.IEnumerable<GlobalModels.ConsultingHours> GetConsultingHours(DateTime From, DateTime To);
        System.Collections.Generic.IEnumerable<GlobalModels.ConsultingHours> GetConsultingHours(int medicineDoctorId);
        System.Collections.Generic.IEnumerable<GlobalModels.ConsultingHours> GetConsultingHours(int medicineDoctorId, DateTime From, DateTime To);
        void AddConsultingHours(GlobalModels.ConsultingHours consultingHours);
        void UpdateConsultingHours(GlobalModels.ConsultingHours consultingHours);
        void DeleteConsultingHours(GlobalModels.ConsultingHours consultigHours, IUnitOfWork uow);
        GlobalModels.ConsultingHours GetConsultingHoursById(int id);

        
        
    }
}
