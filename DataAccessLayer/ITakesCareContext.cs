﻿using System;


namespace DataAccessLayer.Contracts
{
    public interface ITakesCareContext
    {
        System.Data.Entity.DbSet<GlobalModels.ConsultingHours> ConsultingHours { get; set; }
        System.Data.Entity.DbSet<GlobalModels.Appointment> Appointments { get; set; }
        System.Data.Entity.DbSet<GlobalModels.User> Users { get; set; }
        //System.Data.Entity.Infrastructure.DbEntityEntry<T> Entry<T>(T entity);
    }
}
